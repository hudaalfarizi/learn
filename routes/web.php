<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes(['register' => false]);

// Route::group(['middleware' => 'auth'], function () {
//     Route::get('/', 'Dashboard\SuperadminController@index');
// });
Route::get('home', 'Dashboard\SuperadminController@index');
Route::get('profile', 'Auth\UserController@profile');
Route::get('dashboard', 'Dashboard\SuperadminController@home');
Route::get('dashboard/request', 'Dashboard\SuperadminController@permintaan');
Route::get('dashboard/grant', 'Dashboard\SuperadminController@grant');

//Route Login	
Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('validation', 'Auth\LoginController@validation')->name('login.validation');

Route::resource('purchases', 'Purchase\PurchaseController');
Route::resource('grants', 'Grant\GrantController');

Route::group(['prefix' => 'common', 'namespace' => 'Common'], function () {
    Route::post('product-by-id', 'ProductController@getById');
});

Route::group(['prefix' => 'dinas', 'namespace' => 'Dinas'], function () {
    Route::resource('requests', 'RequestController');
    Route::get('request_datatable', 'RequestController@datatable');
    Route::get('request_item_table', 'RequestController@itemDatatables');
    Route::post('requests/create/item', 'RequestController@storeItem');
    Route::delete('requests/item/{id}', 'RequestController@destroyItem');

    Route::resource('distributions', 'DistributionController');
    Route::get('distribution_datatable', 'DistributionController@datatable');
});
