<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'App Covid 19') }}</title>

    @include('partials.admin._css')
</head>
<body class="dashboard-body">
    @include('partials.admin.header')

    @include('partials.admin.content')

    @include('partials.admin.footer')

    @include('partials.admin._js')
</body>
</html>
