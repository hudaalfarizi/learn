<!DOCTYPE html>
<html>
<head>
    <title>Dashboard Admin Covid 19 APD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('partials.home._css')
</head>

<body class="dashboard-body">
    @include('partials.home.header')

    @yield('content')

    @include('partials.home._js')
</body>
</html>
