<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Covid 19 APD App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('partials.login._css')
</head>
<body class="hold-transition login-page">
    <div class="row w-800">
        @include('partials.login.aside')

        @include('partials.login.content')
    </div>
    @include('partials.login._js')
</body>
</html>
