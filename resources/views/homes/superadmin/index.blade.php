@extends('layouts.home')
@section('content')
<div class="footer-container">
    <a class="button-menu" href="{{ url('profile') }}">
        <div class="img-menu">
            <img src="{{ asset('assets/image/profil.png') }}" alt="">
        </div>
        <div class="header-menu">
            MY ACCOUNT
        </div>
        <div class="desc-menu">
            Informasi tentang akun dan data personal anda
        </div>
    </a>
    <a id="h-gradient" class="button-menu" href="{{ url('dashboard') }}">
        <div class="img-menu">
            <img src="{{ asset('assets/image/master.png') }}" alt="">
        </div>
        <div class="header-menu">
            COVID-19 APPs
        </div>
        <div class="desc-menu">
            Pergi ke aplikasi, Untuk menjalankan aktifitas kerja anda
        </div>
    </a>
    <a class="button-menu" href="#">
        <div class="img-menu">
            <img src="{{ asset('assets/image/Master Data White.png') }}" alt="">
        </div>
        <div class="header-menu">
            MASTER DATA
        </div>
        <div class="desc-menu">
            Setting data utama, Untuk memudahkan kinerja sistem
        </div>
    </a>
</div>
@endsection
