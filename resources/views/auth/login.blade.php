@extends('layouts.login')
@section('content')
<form id="form1" style="margin-top: 25px;">
    <div class="label top-7">
        <div class="label-login">Enter Your Username to Login to the application</div>
        <div class="input-group">
            <input type="text" class="form-control-login" name="username" placeholder="Username"
                autocomplete="off">
            <div class="input-group-append no-border">
                <div class="input-group-text border-bottom">
                    <span class="fas fa-user"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="label top-7">
        <div class="label-login">Enter Your Password to Login to the application</div>
        <div class="input-group mb-3">
            <input type="password" class="form-control-login" name="password"
                placeholder="Password" autocomplete="off">
            <div class="input-group-append no-border">
                <div class="input-group-text border-bottom">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
    </div>
    <a class="text-info pull-right" style="font-size: 14px;" href="#">I forgot my
        password</a><br><br>
    <button type="button" class="btn btn-danger pull-right rounded w-100" onclick="validation()">Sign In</button>
    <!-- /.col -->
</form>
@endsection
