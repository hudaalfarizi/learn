@extends('layouts.app')
@section('content')
<h4 class="bold">ACCOUNT</h4>
<div class="flex">
    <div class="f-col-7 f-float-round pad-lg">
        <div class="image-round">
            <label class="profil-init"></label>
            <img src="{{ asset('assets/image/default.png') }}" alt="">
        </div>
        <div class="bg-profil-sec"></div>
    </div>
    <div class="f-col f-float-round padding-tanggal" style="margin-left: 50px;">
        <div class="f-col">
            <div class="darkblue font-md border-bottom bold">DATA UTAMA PENGGUNA</div>
        </div>
        <div class="label pad-xs">
            <div class="title">ID Pengguna</div>
            <div class="desc-clr-sec">ID.....</div>
        </div>
        <div class="label pad-xs">
            <div class="title">Nama</div>
            <div class="desc-clr-sec">Aditya</div>
        </div>
    </div>
</div>
<div class="flex">
    <div class="f-col top-10">
        <div class="f-col">
            <div class="darkblue font-md border-bottom bold">DATA INFORMASI PENGGUNA</div>
        </div>
        <div class="label top-10">
            <div class="title">Email</div>
            <div class="desc">Aditya@gmail.com</div>
        </div>
        <div class="label top-10">
            <div class="title">No Telephone</div>
            <div class="desc"> 0812345678910</div>
        </div>
        <div class="label top-10">
            <div class="title">Username</div>
            <div class="desc">Aditya</div>
        </div>
        <div class="label top-10">
            <div class="title">Passowrd</div>
            <div class="desc">Aditya112233</div>
        </div>
        <div class="label top-10">
            <div class="title">Hak Akses</div>
            <div class="desc">Admin Pusat</div>
        </div>
    </div>
</div>
@endsection
