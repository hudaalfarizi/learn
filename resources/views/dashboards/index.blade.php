@extends('layouts.app')
@section('content')
<h4 class="bold">DASHBOARD</h4>
<div class="date-info-sec padding-tanggal f-green running-date"></div><br />

<div class="flex">
    <div class="f-col-7 f-float-round pad-sm" style="height: 300px;">
        <div class="flex border-bottom mb-2">
            <div class="f-col-4">
                <div class="label">
                    <div class="bold">Stok Kosong</div>
                </div>
            </div>
            <div class="f-col-6 h-margin-r-0">
                <input type="text" class="form-control" placeholder="Cari APD...">
            </div>
        </div>

        <table class="table table-clr mt-3">
            <thead>
                <tr>
                    <th>No</th>
                    <th>ID Produk</th>
                    <th>Nama Produk</th>
                    <th>Kategori</th>
                    <th>QTY Stok</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Brg001</td>
                    <td>Masker APD</td>
                    <td>Masker</td>
                    <td>0 Pcs</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Brg002</td>
                    <td>Helm APD</td>
                    <td>Helm</td>
                    <td>0 Box</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="f-col-3 pad-sm">
        <button class="btn btn-light mb-3" style="box-shadow: 0px 0px 3px #b1b1b1;">
            <a href="{{ url('grants') }}">
                <div class="flex">
                    <div class="f-col-3 mt-2 mr-2">
                        <div class="image-circle-sec">
                            <img src="{{ asset('assets/image/Hibah.png') }}" alt="" style="height: 100%;">
                        </div>
                    </div>
                    <div class="f-col-6">
                        <div class="label-menu">
                            <p>HIBAH</p>
                        </div>
                    </div>
                </div>
            </a>
        </button>

        <button class="btn btn-light" style="box-shadow: 0px 0px 3px #b1b1b1;">
            <a href="{{ url('purchases') }}">
                <div class="flex">
                    <div class="f-col-3 mt-2 mr-2">
                        <div class="image-circle-sec">
                            <img src="{{ asset('assets/image/Pembelian.png') }}" alt="">
                        </div>
                    </div>
                    <div class="f-col-6">
                        <div class="label-menu">
                            <p>PEMBELIAN</p>
                        </div>
                    </div>
                </div>
            </a>
        </button>
    </div>

</div>

<div class="f-float-round padding-tanggal mt-3 pb-3">
    <div class="flex">
        <div class="f-col">
            <h5 class="font-md border-bottom bold">Permintaan APD Provinsi</h5>
        </div>
    </div>
    <br>

    <div class="flex f-float-round pad-md pad-bottom">
        <div class="f-col-3">
            <div class="image-circle">
                <img src="{{ asset('assets/image/photo_default.png') }}" alt="">
            </div>
        </div>
        <div class="f-col-9">
            <div class="flex border-bottom">
                <div class="f-col">
                    <div class="label">
                        <h5 class="bold">KABUPATEN TRENGGALEK</h5>
                        <div class="coral bold" style="font-size: 12px">TR_123165432</div>
                    </div>
                </div>
                <div class="f-col">
                    <a href="{{ url('dashboard/request') }}">
                        <button class="btn btn-outline-info btn-sm pull-right w100">PERMINTAAN</button>
                    </a>
                </div>
                <div class="f-col">
                    <button class="btn btn-warning btn-sm pull-right w100"
                        style="color: white;">MENUNGGU</button>
                </div>
            </div>
            <div class="flex mt-2">
                <div class="f-col">
                    <div class="label">
                        <div>Petugas</div>
                        <div class="darkred">Aditya</div>
                    </div>
                </div>
                <div class="f-col">
                    <div class="label">
                        <div>No. Tlp</div>
                        <div class="darkred">08123456</div>
                    </div>
                </div>
                <div class="f-col">
                    <div class="label">
                        <div>Tanggal Permintaan</div>
                        <div class="darkred">Jumat, 10 April 2020</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="flex f-float-round pad-md pad-bottom">
        <div class="f-col-3">
            <div class="image-circle">
                <img src="{{ asset('assets/image/photo_default.png') }}" alt="">
            </div>
        </div>
        <div class="f-col-9">
            <div class="flex border-bottom">
                <div class="f-col">
                    <div class="label">
                        <h5 class="bold">KOTA MALANG</h5>
                        <div class="coral bold" style="font-size: 12px">TR_123165432</div>
                    </div>
                </div>
                <div class="f-col">
                    <a href="{{ url('dashboard/grant') }}">
                        <button class="btn btn-outline-info btn-sm pull-right w100">HIBAH</button>
                    </a>
                </div>
                <div class="f-col">
                    <button class="btn btn-info btn-sm pull-right w100"
                        style="color: white;">DISTRIBUSI</button>
                </div>
            </div>
            <div class="flex mt-2">
                <div class="f-col">
                    <div class="label">
                        <div>Petugas</div>
                        <div class="darkred">Aditya</div>
                    </div>
                </div>
                <div class="f-col">
                    <div class="label">
                        <div>No. Tlp</div>
                        <div class="darkred">08123456</div>
                    </div>
                </div>
                <div class="f-col">
                    <div class="label">
                        <div>Tanggal Permintaan</div>
                        <div class="darkred">Jumat, 10 April 2020</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
