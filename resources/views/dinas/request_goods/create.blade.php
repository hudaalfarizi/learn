@extends('layouts.dinas')
@section('content')
<h4 class="bold">PERMINTAAN</h4>
<div class="date-info-sec padding-tanggal f-green running-date"></div>

<div class="flex mt-3">
    <div class="f-col top-10">
        <ul class="sub-menu-container">
            <li class="item active"><a href="{{ url('dinas/requests/create') }}">TAMBAH PERMINTAAN</a></li>
            <li class="sep">|</li>
            <li class="item"><a href="{{ url('dinas/requests') }}">DATA PERMINTAAN</a></li>
        </ul>
    </div>
</div>

<div class="flex">
    <div class="f-col">
        <div class="flex label-top">
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Kode Permintaan</div>
                    <input type="text" readonly placeholder="Otomatis dari sistem" class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Hari & Tanggal</div>
                    <input type="text" readonly placeholder="{{ date('Y-m-d') }}" class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label-mid">
                    <div style="font-size: 13px; color: gray;" class="bold">Status Permintaan</div>
                    <input type="text" readonly placeholder="MENUNGGU" class="form-control">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="flex" id="request-good">
    <div class="f-col">
        <div class="flex">
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Permintaan Ke</div>
                    <input type="text" readonly placeholder="{{ $unit }}" class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Nama Petugas</div>
                    <input type="text" name="req_operator" value="{{ ($petugas) ? $petugas->name : '' }}" class="form-control" readonly>
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">No. Tlp</div>
                    <input type="text" name="req_operator_phone" value="{{ ($petugas) ? $petugas->phone : '' }}" class="form-control" readonly>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="flex">
    <div class="f-col">
        <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                  Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my
                  entire
                  soul, like these sweet mornings of spring which I enjoy with my whole heart.
                </div>
    </div>
</div> --}}

<div class="f-float-round padding-tanggal" style="background-color: #0482e3;">
    <div class="flex" id="filter-product">
        <div class="f-col-10">
            <div class="label">
                <div style="font-size: 13px; color: white;" class="bold">Pilih Produk</div>

                {!! Form::select('fil_product', $product, null, ['placeholder' => '-- Produk --', 'class' => 'form-control', 'id' => 'fil_product', 'style' => 'padding: 5px', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="f-col-2">
            <div class="label" style="padding-top: 20px;">
                <button class="btn btn-danger btn-filter-product">LOCK</button>
            </div>
        </div>
        <div class="f-col-4">
            <div class="label">
                <div style="font-size: 13px; color: white;" class="bold">Stok Gudang</div>
                <input type="text" id="fil_stock" readonly class="form-control">
            </div>
        </div>
        <div class="f-col-4">
            <div class="label">
                <div style="font-size: 13px; color: white;" class="bold">Permintaan Stok</div>
                <input type="number" name="fil_request" id="fil_request" placeholder="" class="form-control">
            </div>
        </div>
        <div class="f-col-3">
            <div class="label" style="padding-top: 20px;">
                <button class="btn btn-warning btn-filter-product-save" style="color: white;">SIMPAN</button>
            </div>
        </div>
    </div>
</div>
<div class="flex">
    <table id="table-items" class="table mt-3" style="border-radius: 30px; text-align: center;width: 100%">
        <thead style="background-color: #47a7f7; color: #fff;">
            <tr>
                <th>No</th>
                <th>ID Produk</th>
                <th>Nama Produk</th>
                <th>Permintaan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div class="flex">
    <div class="f-col">
        <button class="btn btn-primary btn-sm"
            style="color: white; margin-left: 85%; height: 50px; width: 100px;" onclick="save();">SIMPAN</button>
    </div>
</div>

@endsection
@push('script')
<script>
    $(function() {
        $('#fil_product').select2({placeholder: '- Pilih -', width:'100%'});

        var table = $('#table-items').DataTable({
            processing : true,
            serverSide : true,
            info: false,
            paging: false,
            lengthChange: false,
            searching: false,
            ordering: false,
            fixedHeader: true,
            ajax : '{{ url('dinas/request_item_table') }}',
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex'},
                { data: 'product_code', name: 'product_code' },
                { data: 'product_name', name: 'product_name' },
                { data: 'reqtemp_qty_request', name: 'reqtemp_qty_request', sClass: 'text-center' },
                { data: 'action', name: 'action'}
            ]
        });

        $('body').on('click', '.btn-filter-product', function(e) {
            e.preventDefault();

            var product_id = $('#fil_product').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: "{{ url('common/product-by-id') }}",
                dataType: 'JSON',
                data: {value : product_id},
                success: function(data) {
                    $('#fil_stock').val(data.prod_stock);
                    $('#fil_request').val('');
                }
            });
        });

        $('body').on('click', '.btn-filter-product-save', function(e) {
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: "{{ url('dinas/requests/create/item') }}",
                dataType: 'JSON',
                data: $('#filter-product input[type=\'number\'], #filter-product select').serialize(),
                success: function(data) {
                    if (data.success === true) {
                        swal('Sukses', 'Data berhasil disimpan', 'success');

                        var table = $('#table-items').DataTable();

                        table.ajax.reload(null, false);

                        $('#fil_product').val(null).trigger('change');

                        $('#fil_stock').val('');

                        $('#fil_request').val('');
                    } else {
                        swal('Error', data.error, 'error');
                    }
                }
            });
        });
    });

    function deleteitem(temp_id)
    {
        swal({
            title: "Hapus",
            text: "Apakah yakin ingin menghapus data ini?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Hapus",
            cancelButtonText: "Batal",
            reverseButtons: !0
        }).then(function (e) {

            if (e.value === true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('dinas/requests/item') }}" + '/' + temp_id,
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success === true) {
                            swal('Sukses', 'Data berhasil dihapus', 'success');

                            var table = $('#table-items').DataTable();

                            table.ajax.reload(null, false);
                        } else {
                            swal('Error', data.error, 'error');
                        }
                    }
                });
            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        });
    }

    function save()
    {
        swal({
            title: "Simpan",
            text: "Apakah yakin ingin menyimpan?",
            type: "info",
            showCancelButton: !0,
            confirmButtonText: "Simpan",
            cancelButtonText: "Batal",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value === true) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "{{ url('dinas/requests') }}",
                    dataType: 'JSON',
                    data: $('#request-good input[type=\'text\']').serialize(),
                    success: function(data) {
                        if (data.success === true) {
                            swal('Sukses', 'Data berhasil disimpan', 'success');

                            location.reload();
                        } else {
                            swal('Error', data.error, 'error');
                        }
                    }
                });
            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        });
    }
</script>
@endpush
