@extends('layouts.dinas')
@section('content')
<h4 class="bold">PERMINTAAN</h4>
<div class="date-info f-green padding-tanggal">
    <a href="{{ url('dinas/requests') }}" class="btn btn-danger rounded pull-right">
        <i class="fas fa-times"></i>
    </a>
</div>
<br>

<div class="flex">
    <div class="f-col">
        <div class="flex label-top">
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Kode Permintaan</div>
                    <input type="text" readonly class="form-control" value="{{ ($data->req_code) ? $data->req_code : '' }}">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Hari & Tanggal</div>
                    <input type="text" readonly class="form-control" {{ ($data->req_date) ? \Carbon\Carbon::parse($data->req_date)->format('d M Y') : '' }}>
                </div>
            </div>
            <div class="f-col-4">
                <div class="label-mid">
                    <div style="font-size: 13px; color: gray;" class="bold">Status Permintaan</div>
                    @php
                        if($data->req_status == 1) {
                            $status = 'Menunggu';
                        } else if($data->req_status == 2) {
                            $status = 'Distribusi';
                        } else {
                            $status = '';
                        }
                    @endphp
                    <input type="text" readonly class="form-control" value="{{ $status }}">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="flex" id="request-good">
    <div class="f-col">
        <div class="flex">
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Permintaan Ke</div>
                    <input type="text" readonly class="form-control" value="{{ config('apd.unit') }}">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Nama Petugas</div>
                    <input type="text" readonly value="{{ ($data->req_operator) ? $data->req_operator : '' }}" class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">No. Tlp</div>
                    <input type="text" readonly value="{{ ($data->req_operator_phone) ? $data->req_operator_phone : '' }}" class="form-control">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="flex">
    <table id="table-items" class="table mt-3" style="border-radius: 30px; text-align: center;width: 100%">
        <thead style="background-color: #47a7f7; color: #fff;">
            <tr>
                <th>No</th>
                <th>ID Produk</th>
                <th>Nama Produk</th>
                <th>Permintaan</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($data->details as $item)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $item->product->prod_code }}</td>
                    <td>{{ $item->product->prod_name }}</td>
                    <td>{{ $item->reqdet_qty_request }} {{ $item->product->prod_unit }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
