@extends('layouts.dinas')
@section('content')
<h4 class="bold">DISTRIBUSI BARANG</h4>
<div class="date-info f-green padding-tanggal">
    <a href="{{ url('dinas/distributions') }}" class="btn btn-danger rounded pull-right">
        <i class="fas fa-times"></i>
    </a>
</div>
<br>

<div class="flex">
    <div class="f-col">
        <div class="flex label-top">
            <div class="f-col-6">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Kode Distribusi</div>
                    <input type="text" readonly class="form-control" value="{{ ($data->distrib_code) ? $data->distrib_code : '' }}">
                </div>
            </div>
            <div class="f-col-6">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Hari & Tanggal</div>
                    <input type="text" readonly class="form-control" value="{{ ($data->distrib_date) ? $tanggal : '' }}">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="flex" id="request-good">
    <div class="f-col">
        <div class="flex">
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Distribusi Ke</div>
                    <input type="text" readonly class="form-control" value="{{ ($data->receiver) ? $data->receiver->wh_name : '' }}">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Nama Petugas</div>
                    <input type="text" readonly value="{{ ($data->distrib_operator) ? $data->distrib_operator : '' }}" class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">No. Tlp</div>
                    <input type="text" readonly value="{{ ($data->distrib_operator_phone) ? $data->distrib_operator_phone : '' }}" class="form-control">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="flex">
    <table id="table-items" class="table mt-3" style="border-radius: 30px; text-align: center;width: 100%">
        <thead style="background-color: #47a7f7; color: #fff;">
            <tr>
                <th>No</th>
                <th>ID Produk</th>
                <th>Nama Produk</th>
                <th>Stok Permintaan</th>
                <th>Stok Barang Yang Diminta</th>
                <th>Realisasi</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($data->details as $item)
                @foreach ($item->request_good->details as $row)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $row->product->prod_code }}</td>
                        <td>{{ $row->product->prod_name }}</td>
                        <td>{{ $row->reqdet_qty_request }} {{ $row->product->prod_unit }}</td>
                        <td>0 {{ $row->product->prod_unit }}</td>
                        <td>{{ $row->reqdet_qty_real }} {{ $row->product->prod_unit }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
</div>
@endsection
