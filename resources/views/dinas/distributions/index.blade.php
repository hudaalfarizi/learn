@extends('layouts.dinas')
@section('content')
<h4 class="bold">DISTRIBUSI BARANG</h4>
<div class="date-info-sec padding-tanggal f-green running-date"></div>

<div class="flex mt-3">
    <div class="f-col top-10">
        <ul class="sub-menu-container">
            <li class="item"><a href="{{ url('dinas/distributions/list') }}">PENERIMAAN PERMINTAAN BARANG</a></li>
            <li class="sep">|</li>
            <li class="item active"><a href="{{ url('dinas/distributions') }}">DATA DISTRIBUSI BARANG</a></li>
        </ul>
    </div>
</div>

<div class="flex mt-2">
    <div class="f-col label-input-title">
        <p class="bold">DATA DISTRIBUSI BARANG</p>
    </div>
</div>

<div class="flex">
    <div class="f-col">
        <table id="table-data" class="table table-hover table-condensed table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Distribusi</th>
                    <th>Penerima</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script>
    $(function() {
        var table = $('#table-data').DataTable({
            processing : true,
            serverSide : true,
            ajax : '{{ url('dinas/distribution_datatable') }}',
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, sClass: 'text-center'},
                { data: 'tanggal', name: 'req_date' },
                { data: 'distrib_code', name: 'distrib_code' },
                { data: 'receiver', name: 'distrib_wh_id_receiver' },
                { data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endpush
