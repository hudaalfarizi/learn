<div class="col-md-6 no-padding">
    <div class="login-box w100">
        <div class="card card-left"
            style="background-image: url('{{ asset('assets/image/card-login.jpg') }}'); background-size: cover; background-position: center;">
            <img class="card-img-left" src="{{ asset('assets/image/logo 2.png') }}" alt="COVID-19">
            <div class="title">
                <div style="font-weight: lighter; font-size: 20px; color: white; margin-top: 6px;">Welcome to
                </div>
                <div style="font-weight: bolder; font-size: 20px;color: white;">COVID-19 Controlling System
                </div>
                <div style="font-weight: lighter; font-size: 20px; color: white; margin-top: 30px;">We rise by
                    lifting
                    others, to building a better world</div>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
</div>
