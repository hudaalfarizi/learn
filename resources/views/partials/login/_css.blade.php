<!-- icheck bootstrap -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/modal.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/loading.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/style.css') }}">
