<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>

<script>
    function validation(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url         : " {{ route('login.validation') }} ",
            data        : $("#form1").serialize(),
            type        : "POST",
            dataType    : "JSON",
            cache       : false,

            beforeSend: function()
            {
                alert('load');
            },
            success: function(response) 
            {
                if(response.res == "success")
                {
                    alert(response.msg);
                    window.location.href = response.url;
                }else if(response.res == "fail")
                {
                    alert(response.msg);
                }
            }
        })
    }
</script>
