<div class="col-md-6 no-padding">
    <div class="login-box w100">
        <div class="card" style="height: 400px; margin-bottom: 0px !important;">
            <div class="card-body login-card-body" style="border-radius: 100px !important;">
                <h3 class="text-left bold" style="margin-bottom: 0px !important;">LOGIN</h3>
                <small class="label-login-head" style="font-size: 12px ;margin-top: 0px !important;">Enter
                    your
                    username and
                    password to be able to log in to the COVID-19 Controlling System
                </small>
                @yield('content')
            </div>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
