<!-- CSS External -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/external/css/bootstrap.min.css') }}">
<!-- CSS Internal -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/dashboard.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/modal.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/loading.css') }}">
