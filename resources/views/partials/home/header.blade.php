<div class="loading">
    <img src="{{ asset('assets/image/asset/loading.gif') }}" alt="">
</div>
<div class="modal-item"></div>
<div class="background-glass"></div>
<div class="header-container">
    <div class="logo-container">
        <img src="{{ asset('assets/image/logo 2.png') }}" alt="">
    </div>
    <div class="logout-container logout-btn" data-link="{{ url('/') }}">
        <img src="{{ asset('assets/image/log out.png') }}" alt="">
        <p>Logout</p>
    </div>
</div>
<div class="content-container">
    <div class="content-component">
        <div class="content-component-text mt-4">
            <div class="text-1">Selamat Datang</div>
            <div class="textc-1">NAMA PENGGUNA</div>
            <div class="text-2">ADMIN PUSAT</div>
            <div class="text-2">APD MANAGEMENT SYSTEM</div>
            <div class="text-1">Semoga Hari Anda Menyenangkan</div>
        </div>
        <br>
        <br>
        <div class="text-3 running-time"></div>
        <div class="text-3 running-date"></div>
    </div>
</div>
