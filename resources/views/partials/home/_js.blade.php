<!-- Javascript External -->
<script src="{{ asset('assets/external/js/jquery.js') }}"></script>
<script src="{{ asset('assets/external/js/bootstrap.min.js') }}"></script>
<!-- Javascript Internal -->
<script src="{{ asset('assets/internal/js/button_action.js') }}"></script>
<script src="{{ asset('assets/internal/js/modal.js') }}"></script>
<script src="{{ asset('assets/internal/js/general.js') }}"></script>
