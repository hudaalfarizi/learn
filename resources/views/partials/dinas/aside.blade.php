<div class="col col-menu pad-sm">
    <div class="menu-logo">
        <img src="{{ asset('assets/image/logo 1.png') }}" alt="" style="padding: 30px;">
    </div>

    <div class="menu-button">
        <a href="#" class="menu-item ">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/Dashboard yellow.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">Dashboard</div>
                <div class="menu-desc">Merupakan Preview Dari Keseluruhan Data Dalam Aplikasi</div>
            </div>
        </a>
    </div>
    <div class="menu-button">
        <a href="#" class="menu-item">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/stok apd Yellow.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">STOK APD</div>
                <div class="menu-desc">Stok Alat Pelindung Diri (APD)</div>
            </div>
        </a>
    </div>
    <div class="menu-button">
        <a href="#" class="menu-item">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/photo_default.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">PENGADAAN BARANG</div>
                <div class="menu-desc">Pengadaan Barang</div>
            </div>
        </a>
    </div>
    <div class="menu-button">
        <a href="#" class="menu-item">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/photo_default.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">PENERIMAAN BARANG</div>
                <div class="menu-desc">Penerimaan Barang</div>
            </div>
        </a>
    </div>
    <div class="menu-button">
        <a href="#" class="menu-item">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/photo_default.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">DISTRIBUSI BARANG</div>
                <div class="menu-desc">Distribusi Barang</div>
            </div>
        </a>
    </div>
    <div class="menu-button">
        <a href="#" class="menu-item active">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/photo_default.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">PERMINTAAN BARANG</div>
                <div class="menu-desc">Permintaan Barang</div>
            </div>
        </a>
    </div>
</div>
