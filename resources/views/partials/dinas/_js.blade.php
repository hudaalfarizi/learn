<!-- Javascript External -->
<script src="{{ asset('assets/external/js/jquery.js') }}"></script>
<script src="{{ asset('assets/external/js/bootstrap.min.js') }}"></script>
{{-- <script src="{{ asset('assets/external/js/highcharts.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts-exporting.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts-export-data.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts-accessibility.js') }}"></script> --}}
<!-- Javascript Internal -->
<script src="{{ asset('assets/internal/js/button_action.js') }}"></script>
<script src="{{ asset('assets/internal/js/modal.js') }}"></script>
<script src="{{ asset('assets/internal/js/general.js') }}"></script>
{{-- <script src="{{ asset('assets/internal/js/line-chart.js') }}"></script> --}}
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

@stack('js')

@stack('script')
