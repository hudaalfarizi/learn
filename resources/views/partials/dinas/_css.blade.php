<!-- CSS External -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/external/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/external/fontawesome-free-5.13.0-web/css/all.css') }}">
<!-- CSS Internal -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/form.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/content.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/button_action.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/modal.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/loading.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">

@stack('css')

@stack('style')
