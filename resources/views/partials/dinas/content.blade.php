<div class="content-container">
    <div class="col col-content padding-content">
        @yield('content')
    </div>

    @include('partials.dinas.aside')
</div>
