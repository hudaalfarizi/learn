<div class="loading">
    <img src="{{ asset('assets/image/loading.gif') }}" alt="">
</div>
<div class="modal-item"></div>

<div class="header-container">
    <div class="rounded c-warning-sec">
        @if (request()->segment(1) == 'profile')
            MY ACCOUNT
        @else
            GO APPLICATION
        @endif
    </div>
    <div class="rounded c-trans">
        <div class="account-img c-trans rounded">
            <label class="account-init"></label>
            <img src="{{ asset('assets/image/default.png') }}" alt="">
        </div>
        <label class="account-name">Aditya / Admin Pusat</label>
    </div>
    <a href="{{ url('home') }}" class="btn btn-danger rounded pull-right">Close</a>
</div>
