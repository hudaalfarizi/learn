<!-- Javascript External -->
<script src="{{ asset('assets/external/js/jquery.js') }}"></script>
<script src="{{ asset('assets/external/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts-exporting.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts-export-data.js') }}"></script>
<script src="{{ asset('assets/external/js/highcharts-accessibility.js') }}"></script>
<!-- Javascript Internal -->
<script src="{{ asset('assets/internal/js/button_action.js') }}"></script>
<script src="{{ asset('assets/internal/js/modal.js') }}"></script>
<script src="{{ asset('assets/internal/js/general.js') }}"></script>
<script src="{{ asset('assets/internal/js/line-chart.js') }}"></script>

@stack('js')

@stack('script')
