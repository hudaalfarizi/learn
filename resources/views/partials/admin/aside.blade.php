<div class="col col-menu pad-sm">
    <div class="menu-logo">
        <img src="{{ asset('assets/image/logo 1.png') }}" alt="" style="padding: 30px;">
    </div>
    <div class="menu-button">
        <a href="{{ asset('dashboard') }}" class="menu-item active">
            <div class="menu-icon">
                <img src="{{ asset('assets/image/dashboard blue.png') }}" alt="">
            </div>
            <div class="menu-text">
                <div class="menu-title">Dashboard</div>
                <div class="menu-desc">Merupakan Preview Dari Keseluruhan Data Dalam Aplikasi</div>
            </div>
        </a>
    </div>
    @if (request()->segment(1) != 'profile')
        <div class="menu-button">
            <a href="#" class="menu-item">
                <div class="menu-icon">
                    <img src="{{ asset('assets/image/stok apd Yellow.png') }}" alt="">
                </div>
                <div class="menu-text">
                    <div class="menu-title">STOK APD</div>
                    <div class="menu-desc">Stok Alat Pelindung Diri (APD)</div>
                </div>
            </a>
        </div>
    @else
        <div class="menu-button">
            <a href="#" class="menu-item">
                <div class="menu-icon">
                    <img src="{{ asset('assets/image/edit profil yellow.png') }}" alt="">
                </div>
                <div class="menu-text">
                    <div class="menu-title">Edit Profile</div>
                    <div class="menu-desc">Merubah profil Pengguna Untuk Keamanan Akun Anda</div>
                </div>
            </a>
        </div>
    @endif
</div>
