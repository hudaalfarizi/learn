<!-- CSS External -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/external/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/external/fontawesome-free-5.13.0-web/css/all.css') }}">
<!-- CSS Internal -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/form.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/content.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/button_action.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/modal.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/internal/css/loading.css') }}">

@stack('css')

@stack('style')
