@extends('layouts.app')
@section('content')
<h4 class="bold">PEMBELIAN</h4>
<div class="date-info f-green padding-tanggal">
    <a href="{{ url('dashboard') }}" class="btn btn-danger rounded pull-right">
        <i class="fas fa-times"></i>
    </a>
</div><br>

<div class="flex">
    <div class="f-col">
        <div class="flex label-top">
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">ID Tiket Pembelian</div>
                    <input type="text" value="" placeholder="TR_123165432 (Auto Skema tree)"
                        class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label">
                    <div style="font-size: 13px; color: gray;" class="bold">Hari & Tanggal</div>
                    <input type="text" value="" placeholder="Jumat, 10 April 2020" class="form-control">
                </div>
            </div>
            <div class="f-col-4">
                <div class="label-mid">
                    <div style="font-size: 13px; color: gray;" class="bold">Jenis Pembelian</div>
                    <select class="form-control" name="" id="">
                        <option value="">Pembelian / Donasi</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="f-float-round padding-tanggal" style="background-color: #0482e3;">
    <div class="flex">
        <div class="f-col-10">
            <div class="label">
                <div style="font-size: 13px; color: white;" class="bold">Pilih Produk</div>
                <input type="text" value="" placeholder="" class="form-control">
            </div>
        </div>
        <div class="f-col-4">
            <div class="label">
                <div style="font-size: 13px; color: white;" class="bold">Quantity</div>
                <input type="text" value="" placeholder="" class="form-control">
            </div>
        </div>
        <div class="f-col-4">
            <div class="label">
                <div style="font-size: 13px; color: white;" class="bold">Satuan</div>
                <input type="text" value="" placeholder="" class="form-control">
            </div>
        </div>
        <div class="f-col-3">
            <div class="label" style="padding-top: 20px;">
                <button class="btn btn-warning" style="color: white;">SIMPAN</button>
            </div>
        </div>
    </div>
</div>


<div class="flex">
    <table class="table mt-3 border-radius: 30px;">
        <thead style="background-color: #47a7f7; color: #fff;">
            <tr>
                <th style="padding-left: 4%;">No</th>
                <th>ID Produk</th>
                <th>Nama Produk</th>
                <th>Quantity</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="padding-left: 4%;">1</td>
                <td>Brg001</td>
                <td>Barang Bagus</td>
                <td>13 Box</td>
                <td>
                    <button class="btn btn-danger btn-sm" style="color: white;">Delete</button>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 4%;">2</td>
                <td>Brg002</td>
                <td>Barang Bagus</td>
                <td>33 Box</td>
                <td>
                    <button class="btn btn-danger btn-sm" style="color: white;">Delete</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="flex">
    <div class="f-col">
        <button class="btn btn-primary btn-sm"
            style="color: white; margin-left: 83%; height: 50px; width: 100px;">SIMPAN</button>
    </div>
</div>
@endsection
