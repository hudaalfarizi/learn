<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('cat_id');
            $table->unsignedInteger('cat_wh_id')->index()->comment('Ref: warehouses');

            $table->string('cat_code')->comment('Auto dari sistem');
            $table->string('cat_code_manual')->nullable()->comment('Diisi dari masing warehouse');
            $table->string('cat_name');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();

            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('prod_id');

            $table->unsignedInteger('prod_cat_id')->index()->comment('Ref: product categories');
            $table->unsignedInteger('prod_wh_id')->index()->comment('Ref: warehouses');

            $table->string('prod_code')->comment('Auto dari sistem');
            $table->string('prod_code_manual')->nullable()->comment('Diisi dari masing warehouse');
            $table->string('prod_name');
            $table->string('prod_unit', 45)->nullable();
            $table->string('prod_photo')->nullable();
            $table->boolean('prod_enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
