<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_units', function (Blueprint $table) {
            $table->increments('whu_id');

            $table->string('whu_code', 45);
            $table->string('whu_name');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();

            $table->timestamps();
        });

        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('wh_id');
            $table->unsignedInteger('wh_whu_id')->index()->comment('Ref: tabel warehouse units');

            $table->unsignedInteger('wh_parent_id')->default(0);

            $table->string('wh_code', 45)->nullable();
            $table->string('wh_name');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_units');
        Schema::dropIfExists('warehouses');
    }
}
