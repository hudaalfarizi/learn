<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_goods', function (Blueprint $table) {
            $table->bigIncrements('req_id');
            $table->unsignedInteger('req_wh_id')->index()->comment('Ref: warehouse');

            $table->string('req_code')->unique();
            $table->string('req_code_manual')->nullable();

            $table->timestamp('req_date');

            $table->unsignedInteger('req_to')->nullable()->comment('Ref: warehouse, 0: jika dia adalah kabupaten');

            $table->string('req_operator')->nullable();
            $table->string('req_operator_phone', 25)->nullable();

            $table->unsignedTinyInteger('req_status')->default(1)->comment('1: Menunggu, 2: Distribusi');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();

            $table->timestamps();
        });

        Schema::create('request_good_details', function (Blueprint $table) {
            $table->string('reqdet_id')->primary();

            $table->unsignedBigInteger('reqdet_req_id')->index()->comment('Ref: request_goods');
            $table->unsignedBigInteger('reqdet_prod_id')->index()->comment('Ref: products');

            $table->unsignedInteger('reqdet_qty_request')->default(0)->comment('Qty Permintaan');
            $table->unsignedInteger('reqdet_qty_real')->default(0)->comment('Qty Realisasi');
            $table->unsignedInteger('reqdet_qty_receive')->default(0)->comment('Qty Realisasi');
            $table->unsignedInteger('reqdet_qty_deficit')->default(0)->comment('Kekurangan/ Selisih');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();

            $table->timestamps();
        });

        Schema::create('request_good_temps', function (Blueprint $table) {
            $table->string('reqtemp_id')->primary();

            $table->unsignedBigInteger('reqtemp_req_id')->index()->comment('Ref: request_goods');
            $table->unsignedBigInteger('reqtemp_prod_id')->index()->comment('Ref: products');
            $table->unsignedBigInteger('reqtemp_user_id')->index();

            $table->unsignedInteger('reqtemp_qty_request')->default(0)->comment('Qty Permintaan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_goods');
        Schema::dropIfExists('request_good_details');
    }
}
