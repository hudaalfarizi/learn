<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributions', function (Blueprint $table) {
            $table->bigIncrements('distrib_id');
            $table->string('distrib_code');
            $table->unsignedInteger('distrib_wh_id_sender')->index()->comment('Ref Tabel Warehouse. Pengirim distribusi');
            $table->unsignedInteger('distrib_wh_id_receiver')->index()->comment('Ref Tabel Warehouse');
            $table->timestamp('distrib_date');
            $table->string('distrib_operator')->nullable();
            $table->string('distrib_operator_phone', 25)->nullable();
            $table->unsignedTinyInteger('distrib_status')->nullable()->comment('0: Menunggu, 1: Distribusi');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();
            $table->timestamps();
        });

        Schema::create('distribution_details', function (Blueprint $table) {
            $table->string('distribdet_id')->primary();

            $table->unsignedBigInteger('distribdet_distrib_id')->index()->comment('Ref tabel distributions');
            $table->unsignedBigInteger('distribdet_req_id')->index()->comment('Ref tabel request goods');
            $table->string('distribdet_reqdet_id')->index()->nullable()->comment('Ref tabel request good details');

            $table->string('created_by', 45)->nullable();
            $table->string('updated_by', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributions');
        Schema::dropIfExists('distribution_details');
    }
}
