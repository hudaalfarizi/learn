<?php

return [
    'role_structure' => [
        'superadmin' => [
            'dashboard-superadmin' => 'r',
            'auth-users' => 'c,r,u,d',
            'auth-roles' => 'c,r,u,d',
            'auth-permissons' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'administrator' => [
            'dashboard-admin' => 'c,r,u,d',
            'auth-users' => 'c,r,u,d',
            'auth-roles' => 'c,r,u,d',
            'auth-permissons' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'petugas' => [
            'dashboard-petugas' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',
        'a' => 'approve',
        'e' => 'export',
        'i' => 'import'
    ]
];
