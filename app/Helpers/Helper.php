<?php

if (!function_exists('format_date')) {
    /**
     * Untuk format tanggal dengan hari dalam indonesia
     *
     * @return string a string nilai rupiah
     *
     * */
    function format_date_day($date)
    {
        $carbon = new \Carbon\Carbon();

        $date = $carbon->parse($date)->format('Y-m-d');

        $day_list = [
            'Sunday' => 'Minggu',
            'Monday' => 'Senin',
            'Tuesday' => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday' => 'Kamis',
            'Friday' => 'Jumat',
            'Saturday' => 'Sabtu'
        ];

        $day = date('l', strtotime($date));

        $month_list = [
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ];

        $pecahkan = explode('-', $date);

        $tanggal = $pecahkan[2] . ' ' . $month_list[ (int) $pecahkan[1] ] . ' ' . $pecahkan[0];

        return $day_list[$day] . ', '. $tanggal;
    }
}
