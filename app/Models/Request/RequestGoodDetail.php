<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class RequestGoodDetail extends Model
{
    protected $table = 'request_good_details';

    protected $primaryKey = 'reqdet_id';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reqdet_req_id', 'reqdet_prod_id', 'reqdet_qty_request', 'reqdet_qty_real', 'reqdet_qty_receive','reqdet_qty_deficit', 'created_by', 'updated_by'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->reqdet_id = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product', 'reqdet_prod_id', 'prod_id');
    }
}
