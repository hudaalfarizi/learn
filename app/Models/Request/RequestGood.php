<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Model;

class RequestGood extends Model
{
    protected $table = 'request_goods';

    protected $primaryKey = 'req_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'req_wh_id', 'req_code', 'req_code_manual', 'req_date', 'req_to', 'req_operator', 'req_operator_phone', 'req_status',  'created_by', 'updated_by'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\Request\RequestGoodDetail', 'reqdet_req_id', 'req_id');
    }
}
