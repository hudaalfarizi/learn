<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class RequestGoodTemp extends Model
{
    protected $table = 'request_good_temps';

    protected $primaryKey = 'reqtemp_id';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reqtemp_req_id', 'reqtemp_prod_id', 'reqtemp_user_id', 'reqtemp_qty_request'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->reqtemp_id = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product', 'reqtemp_prod_id', 'prod_id');
    }
}
