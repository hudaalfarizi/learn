<?php

namespace App\Models\Distribution;

use Illuminate\Database\Eloquent\Model;

class Distribution extends Model
{
    protected $table = 'distributions';

    protected $primaryKey = 'distrib_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'distrib_code', 'distrib_wh_id_sender', 'distrib_wh_id_receiver', 'distrib_date', 'distrib_operator', 'distrib_operator_phone', 'distrib_status', 'created_by', 'updated_by'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\Distribution\DistributionDetail', 'distribdet_distrib_id', 'distrib_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\Warehouse\Warehouse', 'distrib_wh_id_receiver', 'wh_id');
    }
}
