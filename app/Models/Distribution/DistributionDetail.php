<?php

namespace App\Models\Distribution;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class DistributionDetail extends Model
{
    protected $table = 'distribution_details';

    protected $primaryKey = 'distribdet_id';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'distribdet_distrib_id', 'distribdet_req_id', 'distribdet_reqdet_id', 'created_by', 'updated_by'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->distribdet_id = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

    public function distribution()
    {
        return $this->belongsTo('App\Models\Distribution\Distribution', 'distribdet_distrib_id', 'distrib_id');
    }

    public function request_good()
    {
        return $this->belongsTo('App\Models\Request\RequestGood', 'distribdet_req_id', 'req_id');
    }
}
