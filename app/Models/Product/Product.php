<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $primaryKey = 'prod_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_cat_id', 'prod_wh_id', 'prod_code', 'prod_code_manual', 'prod_name', 'prod_unit', 'prod_photo', 'prod_stock', 'prod_enabled', 'created_by', 'updated_by'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
