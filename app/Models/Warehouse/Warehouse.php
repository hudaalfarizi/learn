<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = 'warehouses';

    protected $primaryKey = 'wh_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wh_whu_id', 'wh_parent_id', 'wh_code', 'wh_name', 'created_by', 'updated_by'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
