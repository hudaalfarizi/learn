<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function validation(Request $request)
    {
        $feedback['res'] = '';
        $feedback['msg'] = '';
        $islogin = [
            'username' => $request->username,
            'password' => $request->password
        ];
        if(Auth::attempt($islogin))
        {
            $feedback['res'] = 'success';
            $feedback['msg'] = 'Anda berhasil login';

        }else{
            $feedback['res'] = 'fail';
            $feedback['msg'] = 'Anda gagal login';
        }
        echo json_encode($feedback);
    }
}
