<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SuperadminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('homes.superadmin.index');
    }

    public function home()
    {
        return view('dashboards.index');
    }

    public function permintaan()
    {
        return view('dashboards.request');
    }

    public function grant()
    {
        return view('dashboards.grant');
    }
}
