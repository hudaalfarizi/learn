<?php

namespace App\Http\Controllers\Dinas;

use App\Http\Controllers\Controller;
use App\Models\Distribution\Distribution;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class DistributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dinas.distributions.index');
    }

    public function datatable(Request $request)
    {
        $wh_id = 2;

        $select = Distribution::where('distrib_wh_id_sender', $wh_id);

        $data = Datatables::of($select)
            ->addIndexColumn()
            ->addColumn('tanggal', function($select) {
                return Carbon::parse($select->distrib_date)->format('d M Y');
            })
            ->addColumn('receiver', function($select) {
                return $select->receiver->wh_name;
            })
            ->addColumn('action', function($select) {
                $action = '
                    <a href="'.url('dinas/distributions/'. $select->distrib_id).'" class="btn btn-sm btn-primary w70">Detail</a>
                ';

                return $action;
            })
            ->rawColumns([
                'tanggal',
                'receiver',
                'action',
            ]);

        return $data->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(empty($id) || !$id) {
            return redirect('dinas/distributions');
        }

        $data = Distribution::findOrFail($id);

        $tanggal = format_date_day($data->distrib_date);

        return view('dinas.distributions.show', compact('data', 'tanggal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
