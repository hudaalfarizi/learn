<?php

namespace App\Http\Controllers\Dinas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Product\Product;
use App\Models\Request\RequestGood;
use App\Models\Request\RequestGoodDetail;
use App\Models\Request\RequestGoodTemp;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dinas.request_goods.index');
    }

    public function datatable(Request $request)
    {
        $wh_id = 1;

        $select = RequestGood::where('req_wh_id', $wh_id);

        // dd($select);

        $data = Datatables::of($select)
            ->addIndexColumn()
            ->addColumn('tanggal', function($select) {
                return Carbon::parse($select->req_date)->format('d M Y');
            })
            ->addColumn('action', function($select) {
                $action = '
                    <a href="'.url('dinas/requests/'. $select->req_id).'" class="btn btn-sm btn-primary w70">Detail</a>
                ';

                return $action;
            })
            ->rawColumns([
                'tanggal',
                'action',
            ]);

        return $data->make(true);
    }

    public function itemDatatables(Request $request)
    {
        $id = 1;

        $select = RequestGoodTemp::where('reqtemp_user_id', $id);

        $data = Datatables::of($select)
            ->addIndexColumn()
            ->addColumn('nomor', function($select) {
                return 1;
            })
            ->addColumn('product_code',  function($select) {
                return $select->product->prod_code;
            })
            ->addColumn('product_name',  function($select) {
                return $select->product->prod_name;
            })
            ->addColumn('action', function($select) {
                $action = "
                    <a onclick=\"deleteitem('".$select->reqtemp_id."');\" class='btn btn-danger'>Delete</a>
                ";

                return $action;
            })
            ->rawColumns([
                'nomor',
                'product_code',
                'product_name',
                'action',
            ]);

        return $data->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit = config('apd.unit');

        $id = 1;

        // Petugas
        $petugas = User::select('name', 'phone')
            ->where('warehouse_id', 0)
            ->where('enabled', true)
            ->whereHas('roles', $filter = function($q) {
                $q->where('name', 'petugas');
            })->with(['roles' => $filter])->first();

        $product = Product::where('prod_wh_id', 0)->where('prod_enabled', true)->pluck('prod_name', 'prod_id')->toArray();

        return view('dinas.request_goods.create', compact('unit', 'petugas', 'product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeItem(Request $request)
    {
        try {
            $request['reqtemp_user_id'] = 1;

            $request['reqtemp_prod_id'] = $request->fil_product;

            $request['reqtemp_qty_request'] = $request->fil_request;

            $create = RequestGoodTemp::create($request->all());

            return response()->json(['success' => true]);

            // toastr()->success('Data berhasil ditambahkan', 'Berhasil');

            // return redirect('master/ads');
        } catch (QueryException $qe) {
            // toastr()->error($qe->getMessage() . ' ' . $qe->getLine(), 'Error');

            // return redirect()->back()->withInput();
        } catch (ModelNotFoundException $mnfe) {
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            // toastr()->error($e->getMessage() . ' ' . $e->getLine(), 'Error');

            // return redirect()->back()->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $id = 1;

            $request['req_code'] = '111';

            $request['req_wh_id'] = 1;

            $request['req_date'] = Carbon::now();

            $request['req_to'] = 0;

            $request['created_by'] = 'Dinas';

            $store = RequestGood::create($request->all());

            $items = RequestGoodTemp::where('reqtemp_user_id', $id)->get();

            if($items) {
                foreach ($items as $key => $item) {
                    $data = [
                        'reqdet_req_id' => $store->req_id,
                        'reqdet_prod_id' => $item->reqtemp_prod_id,
                        'reqdet_qty_request' => $item->reqtemp_qty_request,
                        'created_by' => 'Dinas'
                    ];

                    $store_detail = RequestGoodDetail::create($data);
                }

                if($store_detail) {
                    RequestGoodTemp::where('reqtemp_user_id', $id)->delete();
                }

                DB::commit();
            }

            alert()->success('Hapus data berhasil', 'Sukses');

            return response()->json(['success' => true]);
        } catch (QueryException $qe) {
            DB::rollback();

            return response()->json(['error' => $qe->getMessage()]);
        } catch (ModelNotFoundException $mnfe) {
            DB::rollback();

            return response()->json(['error' => $mnfe->getMessage()]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = RequestGood::findOrFail($id);

        return view('dinas.request_goods.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyItem($id)
    {
        try {
            $delete = RequestGoodTemp::find($id)->delete();

            alert()->success('Hapus data berhasil', 'Sukses');

            return response()->json(['success' => true]);
        } catch (QueryException $qe) {
            // toastr()->error($qe->getMessage() . ' ' . $qe->getLine(), 'Error');

            // return redirect()->back()->withInput();
        } catch (ModelNotFoundException $mnfe) {
            // toastr()->error($mnfe->getMessage() . ' ' . $mnfe->getLine(), 'Error');

            // return redirect()->back()->withInput();
        } catch (\Exception $e) {
            // toastr()->error($e->getMessage() . ' ' . $e->getLine(), 'Error');

            // return redirect()->back()->withInput();
        }
    }
}
